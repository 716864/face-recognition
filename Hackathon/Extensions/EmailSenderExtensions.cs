using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Hackathon.Services;

namespace Hackathon.Services
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailAsync(email, "Confirme seu e-mail",
                $"Por favor confirme o cadastro de sua conta, clicando no link a seguir: <a href='{HtmlEncoder.Default.Encode(link)}'>link</a>");
        }
    }
}
