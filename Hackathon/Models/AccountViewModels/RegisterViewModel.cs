﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Hackathon.CustomAttributes;

namespace Hackathon.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [CustomRequired]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [CustomRequired]
        [StringLength(100, ErrorMessage = "A {0} deve ter pelo menos {2} e no máximo {1} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme a senha")]
        [Compare("Password", ErrorMessage = "A senha e confirmação de senha estão diferentes.")]
        public string ConfirmPassword { get; set; }
    }
}
