﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Hackathon.CustomAttributes;

namespace Hackathon.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [CustomRequired]
        [EmailAddress]
        public string Email { get; set; }
    }
}
