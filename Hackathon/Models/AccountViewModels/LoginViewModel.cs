﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Hackathon.CustomAttributes;

namespace Hackathon.Models.AccountViewModels
{
  public class LoginViewModel
  {
    [CustomRequired]
    [EmailAddress]
    public string Email { get; set; }

    [CustomRequired]
    [DataType(DataType.Password)]
    [Display(Name = "Senha")]
    public string Password { get; set; }

    [Display(Name = "Lembrar-me?")]
    public bool RememberMe { get; set; }
  }
}
