﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Hackathon.CustomAttributes;

namespace Hackathon.Models.AccountViewModels
{
    public class LoginWithRecoveryCodeViewModel
    {
            [CustomRequired]
            [DataType(DataType.Text)]
            [Display(Name = "Recovery Code")]
            public string RecoveryCode { get; set; }
    }
}
