﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.CustomAttributes
{

  public class CustomRequired : RequiredAttribute
  {
    public CustomRequired()
    {
      ErrorMessage = "O campo {0} é obrigatório.";
    }
  }
}
