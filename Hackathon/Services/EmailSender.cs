﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.Services
{
  // This class is used by the application to send email for account confirmation and password reset.
  // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
  public class EmailSender : IEmailSender
  {
    public Task SendEmailAsync(string email, string subject, string message)
    {
      // Credentials:
      string sendGridUserName = "azure_f7cecada68390a44c06ff057ade219cb@azure.com";
      string sendGridPassword = "Junt0sN@T1";
      string sentFrom = "naoresponder@juntosnati.com";

      // Configure the client
      var client = new System.Net.Mail.SmtpClient("smtp.sendgrid.net", 587);

      client.Port = 587;
      client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
      client.UseDefaultCredentials = false;

      // Create the credentials:
      System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);

      //client.EnableSsl = true;
      client.Credentials = credentials;

      // Create the message:
      var mail = new System.Net.Mail.MailMessage(sentFrom, email);

      mail.Subject = subject;
      mail.Body = message;
      mail.IsBodyHtml = true;

      // Send:
      return client.SendMailAsync(mail);
    }
  }
}
